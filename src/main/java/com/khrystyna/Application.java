package com.khrystyna;

import com.khrystyna.services.TerminalService;
import com.khrystyna.services.NumberService;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Main service.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 01.04.2019
 */
public class Application {
    /**
     * Loading time for the program to pause and then continue to work.
     */
    private static final int LOADING_DELAY = 1000;
    /**
     * Time for loading update cycle.
     */
    private static final int UPDATE_TIME = 100;
    /**
     * Service for reading user inputs.
     */
    private TerminalService terminalService = new TerminalService();
    /**
     * Calculating service.
     */
    private NumberService numberService = new NumberService();

    /**
     * Main method that runs when the program is started.
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        Application app = new Application();

        app.printLoading();
        app.run();
    }

    /**
     * Starts communicating with user and give answers due to typed values.
     */
    private void run() {
        System.out.print("Enter the interval limits"
                + "\nfrom: ");
        int start = terminalService.getIntegerInput();
        System.out.print("to: ");
        int end = terminalService.getIntegerInput();

        int[] oddNumbers = numberService.getOddNumbersFrom(start, end);
        System.out.print("Odd numbers: " + Arrays.toString(oddNumbers));
        System.out.println("\nSum: " + IntStream.of(oddNumbers).sum());

        int[] evenNumbers = numberService.getEvenNumbersFrom(start, end);
        System.out.print("\nEven numbers: " + Arrays.toString(evenNumbers));
        System.out.println("\nSum: " + IntStream.of(evenNumbers).sum());

        System.out.print("\nEnter size of set: ");
        int setSize = terminalService.getIntegerInput();

        int[] fibonacciNumbers = numberService
                .getFibonacciNumbers(oddNumbers[oddNumbers.length - 1],
                        evenNumbers[0], setSize);

        System.out.println("Fibonacci numbers: "
                + Arrays.toString(fibonacciNumbers));

        System.out.print("Do you want to continue(y/n)? ");
        char characterInput = terminalService.getCharacterInput();
        if (characterInput == 'y' || characterInput == 'Y') {
            System.out.println();
            run();
        }
    }

    /**
     * Pauses program for certain time and every cycle prints dot symbol '.'.
     */
    private void printLoading() {
        try {
            System.out.print("Loading...");
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < Application.LOADING_DELAY) {
                Thread.sleep(Application.UPDATE_TIME);
                System.out.print('.');
            }
            System.out.println('\n');
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
