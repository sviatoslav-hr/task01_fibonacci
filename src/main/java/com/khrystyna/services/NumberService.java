package com.khrystyna.services;

import java.util.stream.IntStream;

/**
 * A simple service that performs numerical tasks.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 01.04.2019
 */
public class NumberService {

    /**
     * Gets odd numbers due to interval.
     *
     * @param start start of the interval
     * @param end   end of the interval
     * @return odd numbers from start to the end of interval
     */
    public final int[] getOddNumbersFrom(int start, int end) {
        if (start > end) {
            int temp = end;
            end = start;
            start = temp;
        }
        return IntStream
                .range(start, end + 1)
                .filter(value -> value % 2 == 1 || value % 2 == -1)
                .toArray();
    }

    /**
     * Gets even numbers due to interval in reverse.
     *
     * @param start start of the interval
     * @param end   end of the interval
     * @return even numbers from end to the start of interval
     */
    public final int[] getEvenNumbersFrom(int start, int end) {
        if (start > end) {
            int temp = end;
            end = start;
            start = temp;
        }
        int arraySize = (end - start) / 2;
        if (end - start % 2 == 1 && end % 2 == 0) {
            arraySize++;
        }
        int[] array = new int[arraySize];

        for (int i = 0, currentNumber = end; i < arraySize
                && currentNumber >= start; currentNumber--) {
            if (currentNumber % 2 == 0) {
                array[i] = currentNumber;
                i++;
            }
        }
        return array;
    }

    /**
     * Generates a certain number of numbers in the Fibonacci series.
     *
     * @param f1      first element of Fibonacci set
     * @param f2      second element of Fibonacci set
     * @param setSize size of Fibonacci set
     * @return Fibonacci numbers array of certain size generated from first two
     * elements
     */
    public final int[] getFibonacciNumbers(final int f1, final int f2,
                                           final int setSize) {
        if (setSize < 1) {
            return null;
        }
        int[] fibonacciNumbers = new int[setSize];

        fibonacciNumbers[0] = f1;
        if (setSize == 1) {
            return fibonacciNumbers;
        } else {
            fibonacciNumbers[1] = f2;
        }
        for (int i = 2; i < setSize; i++) {
            fibonacciNumbers[i] = fibonacciNumbers[i - 1]
                    + fibonacciNumbers[i - 2];
        }
        return fibonacciNumbers;
    }
}
