package com.khrystyna.services;

import java.util.Scanner;

/**
 * A simple service that can pass input from terminal.
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 01.04.2019
 */
public class TerminalService {
    /**
     * Scanner that allows to read an input from System.in.
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * @return int value typed from console.
     */
    public final int getIntegerInput() {
        return scanner.nextInt();
    }

    /**
     * @return char value typed from console
     */
    public final char getCharacterInput() {
        return scanner.next().charAt(0);
    }
}
